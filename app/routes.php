<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('embed/{id}', function($id){
  return Response::view('embed', compact('id'))
          ->header('content_type', 'text/javascript');
}); 

Route::get('status/{id}', function($id){
  return array('online' => true);
});