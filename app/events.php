<?php
Event::listen('generic.event',function($client_data){
    echo print_r($client_data);
    return BrainSocket::message('generic.event',array('message'=>$client_data->data->message,'user'=>$client_data->data->user));
});

Event::listen('app.success',function($client_data){
    return BrainSocket::success(array('There was a Laravel App Success Event!'));
});

Event::listen('app.error',function($client_data){
    return BrainSocket::error(array('There was a Laravel App Error!'));
});
