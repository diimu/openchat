(function(e){if(typeof define=="function"){define(e)}else if(typeof module!="undefined"){module.exports=e}else{this.qwest=e}})(function(){var win=window,limit=null,requests=0,request_stack=[],getXHR=function(){return win.XMLHttpRequest?new XMLHttpRequest:new ActiveXObject("Microsoft.XMLHTTP")},version2=getXHR().responseType==="",qwest=function(method,url,data,options,before){data=data||null;options=options||{};var typeSupported=false,xhr=getXHR(),async=options.async===undefined?true:!!options.async,cache=options.cache,type=options.type?options.type.toLowerCase():"json",user=options.user||"",password=options.password||"",headers={"X-Requested-With":"XMLHttpRequest"},accepts={xml:"application/xml, text/xml",html:"text/html",json:"application/json, text/javascript",js:"application/javascript, text/javascript"},toUpper=function(e,t,n){return t+n.toUpperCase()},vars="",i,j,parseError="parseError",serialized,success_stack=[],error_stack=[],complete_stack=[],response,success,error,func,promises={success:function(e){if(async){success_stack.push(e)}else if(success){e.apply(xhr,[response])}return promises},error:function(e){if(async){error_stack.push(e)}else if(error){e.apply(xhr,[response])}return promises},complete:function(e){if(async){complete_stack.push(e)}else{e.apply(xhr)}return promises}},promises_limit={success:function(e){request_stack[request_stack.length-1].success.push(e);return promises_limit},error:function(e){request_stack[request_stack.length-1].error.push(e);return promises_limit},complete:function(e){request_stack[request_stack.length-1].complete.push(e);return promises_limit}},handleResponse=function(){var i,req,p;--requests;if(request_stack.length){req=request_stack.shift();p=qwest(req.method,req.url,req.data,req.options,req.before);for(i=0;func=req.success[i];++i){p.success(func)}for(i=0;func=req.error[i];++i){p.error(func)}for(i=0;func=req.complete[i];++i){p.complete(func)}}try{if(xhr.status!=200){throw xhr.status+" ("+xhr.statusText+")"}var responseText="responseText",responseXML="responseXML";if(typeSupported&&xhr.response!==undefined){response=xhr.response}else{switch(type){case"json":try{if(win.JSON){response=win.JSON.parse(xhr[responseText])}else{response=eval("("+xhr[responseText]+")")}}catch(e){throw"Error while parsing JSON body"}break;case"js":response=eval(xhr[responseText]);break;case"xml":if(!xhr[responseXML]||xhr[responseXML][parseError]&&xhr[responseXML][parseError].errorCode&&xhr[responseXML][parseError].reason){throw"Error while parsing XML body"}else{response=xhr[responseXML]}break;default:response=xhr[responseText]}}success=true;if(async){for(i=0;func=success_stack[i];++i){func.apply(xhr,[response])}}}catch(e){error=true;response="Request to '"+url+"' aborted: "+e;if(async){for(i=0;func=error_stack[i];++i){func.apply(xhr,[response])}}}if(async){for(i=0;func=complete_stack[i];++i){func.apply(xhr)}}};if(limit&&requests==limit){request_stack.push({method:method,url:url,data:data,options:options,before:before,success:[],error:[],complete:[]});return promises_limit}++requests;if(win.ArrayBuffer&&(data instanceof ArrayBuffer||data instanceof Blob||data instanceof Document||data instanceof FormData)){if(method=="GET"){data=null}}else{var values=[],enc=encodeURIComponent;for(i in data){if(data[i]!==undefined){values.push(enc(i)+(data[i].pop?"[]":"")+"="+enc(data[i]))}}data=values.join("&");serialized=true}if(method=="GET"){vars+=data}if(cache==null){cache=method=="POST"}if(!cache){if(vars){vars+="&"}vars+="__t="+Date.now()}if(vars){url+=(/\?/.test(url)?"&":"?")+vars}xhr.open(method,url,async,user,password);if(type&&version2){try{xhr.responseType=type;typeSupported=xhr.responseType==type}catch(e){}}if(version2){xhr.onload=handleResponse}else{xhr.onreadystatechange=function(){if(xhr.readyState==4){handleResponse()}}}for(i in headers){j=i.replace(/(^|-)([^-])/g,toUpper);headers[j]=headers[i];delete headers[i];xhr.setRequestHeader(j,headers[j])}if(!headers["Content-Type"]&&serialized&&method=="POST"){xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded")}if(!headers.Accept){xhr.setRequestHeader("Accept",accepts[type])}if(before){before.apply(xhr)}xhr.send(method=="POST"?data:null);return promises};return{get:function(e,t,n,r){return qwest("GET",e,t,n,r)},post:function(e,t,n,r){return qwest("POST",e,t,n,r)},xhr2:version2,limit:function(e){limit=e}}}())
function BrainSocketPubSub(){this.subscriptions=[];this.forget=function(e){for(x=0;x<this.subscriptions.length;x++){if(this.subscriptions[x].name==e[0],this.subscriptions[x].callback==e[1])this.subscriptions.splice(x,1)}};this.listen=function(e,t){this.subscriptions.push({name:e,callback:t});return[e,t]};this.fire=function(e,t){var n=[];if(this.subscriptions.length>0){for(var r=0;r<this.subscriptions.length;r++){if(this.subscriptions[r].name==e)n.push({fn:this.subscriptions[r].callback})}for(r=0;r<n.length;r++){n[r].fn.apply(this,[t])}}}}function BrainSocket(e,t){this.connection=e;this.Event=t;this.connection.BrainSocket=this;this.connection.digestMessage=function(e){try{var t=JSON.parse(e);if(t.server&&t.server.event){this.BrainSocket.Event.fire(t.server.event,t)}else{this.BrainSocket.Event.fire(t.client.event,t)}}catch(n){this.BrainSocket.Event.fire(e)}};this.connection.onerror=function(e){console.log(e)};this.connection.onmessage=function(e){this.digestMessage(e.data)};this.success=function(e){this.message("app.success",e)};this.error=function(e){this.message("app.error",e)};this.message=function(e,t){var n={client:{}};n.client.event=e;if(!t){t=[]}n.client.data=t;this.connection.send(JSON.stringify(n))}}

var id = {{$id}}, embed, css;
reset = '.openchat_container_{{$id}}{}.openchat_container_{{$id}} html,.openchat_container_{{$id}} body,.openchat_container_{{$id}} div,.openchat_container_{{$id}} span,.openchat_container_{{$id}} applet,.openchat_container_{{$id}} object,.openchat_container_{{$id}} iframe,.openchat_container_{{$id}} h1,.openchat_container_{{$id}} h2,.openchat_container_{{$id}} h3,.openchat_container_{{$id}} h4,.openchat_container_{{$id}} h5,.openchat_container_{{$id}} h6,.openchat_container_{{$id}} p,.openchat_container_{{$id}} blockquote,.openchat_container_{{$id}} pre,.openchat_container_{{$id}} a,.openchat_container_{{$id}} abbr,.openchat_container_{{$id}} acronym,.openchat_container_{{$id}} address,.openchat_container_{{$id}} big,.openchat_container_{{$id}} cite,.openchat_container_{{$id}} code,.openchat_container_{{$id}} del,.openchat_container_{{$id}} dfn,.openchat_container_{{$id}} em,.openchat_container_{{$id}} img,.openchat_container_{{$id}} ins,.openchat_container_{{$id}} kbd,.openchat_container_{{$id}} q,.openchat_container_{{$id}} s,.openchat_container_{{$id}} samp,.openchat_container_{{$id}} small,.openchat_container_{{$id}} strike,.openchat_container_{{$id}} strong,.openchat_container_{{$id}} sub,.openchat_container_{{$id}} sup,.openchat_container_{{$id}} tt,.openchat_container_{{$id}} var,.openchat_container_{{$id}} b,.openchat_container_{{$id}} u,.openchat_container_{{$id}} i,.openchat_container_{{$id}} center,.openchat_container_{{$id}} dl,.openchat_container_{{$id}} dt,.openchat_container_{{$id}} dd,.openchat_container_{{$id}} ol,.openchat_container_{{$id}} ul,.openchat_container_{{$id}} li,.openchat_container_{{$id}} fieldset,.openchat_container_{{$id}} form,.openchat_container_{{$id}} label,.openchat_container_{{$id}} legend,.openchat_container_{{$id}} table,.openchat_container_{{$id}} caption,.openchat_container_{{$id}} tbody,.openchat_container_{{$id}} tfoot,.openchat_container_{{$id}} thead,.openchat_container_{{$id}} tr,.openchat_container_{{$id}} th,.openchat_container_{{$id}} td,.openchat_container_{{$id}} article,.openchat_container_{{$id}} aside,.openchat_container_{{$id}} canvas,.openchat_container_{{$id}} details,.openchat_container_{{$id}} embed,.openchat_container_{{$id}} figure,.openchat_container_{{$id}} figcaption,.openchat_container_{{$id}} footer,.openchat_container_{{$id}} header,.openchat_container_{{$id}} hgroup,.openchat_container_{{$id}} menu,.openchat_container_{{$id}} nav,.openchat_container_{{$id}} output,.openchat_container_{{$id}} ruby,.openchat_container_{{$id}} section,.openchat_container_{{$id}} summary,.openchat_container_{{$id}} time,.openchat_container_{{$id}} mark,.openchat_container_{{$id}} audio,.openchat_container_{{$id}} video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}.openchat_container_{{$id}} article,.openchat_container_{{$id}} aside,.openchat_container_{{$id}} details,.openchat_container_{{$id}} figcaption,.openchat_container_{{$id}} figure,.openchat_container_{{$id}} footer,.openchat_container_{{$id}} header,.openchat_container_{{$id}} hgroup,.openchat_container_{{$id}} menu,.openchat_container_{{$id}} nav,.openchat_container_{{$id}} section{display:block}.openchat_container_{{$id}} body{line-height:1}.openchat_container_{{$id}} ol,.openchat_container_{{$id}} ul{list-style:none}.openchat_container_{{$id}} blockquote,.openchat_container_{{$id}} q{quotes:none}.openchat_container_{{$id}} blockquote:before,.openchat_container_{{$id}} blockquote:after,.openchat_container_{{$id}} q:before,.openchat_container_{{$id}} q:after{content:\'\';content:none}.openchat_container_{{$id}} table{border-collapse:collapse;border-spacing:0}';
css = reset+'.openchat_container_{{$id}}{position: fixed;bottom: 0;right: 0;}#openchat_hidden_{{$id}}{display:none}';
embed = '<style>'+css+'</style><div class="openchat_container_{{$id}}"><a id="openchat_button_{{$id}}" href="#">En que te podemos ayudar?</a><div id="openchat_hidden_{{$id}}"></div></div>';

//first write the code, then attach the events
document.write(embed);

function waitWS(socket, callback){
    setTimeout(
        function () {
            if (socket.readyState === 1) {
                console.log("Connection is made")
                if(callback != null){
                    callback();
                }
                return;

            } else {
                console.log("wait for connection...")
                waitWS(socket);
            }

        }, 5); // wait 5 milisecond for the connection...
}

//dom management
var link = document.getElementById('openchat_button_{{$id}}');
var hidden = document.getElementById('openchat_hidden_{{$id}}');

link.onclick = function(){
  displayInit(hidden);
  return false;
}

display = function(e,t,c){
  var cssHidden = window.getComputedStyle(e),
      cssHiddenDisplay = cssHidden.getPropertyValue('display');

  if(cssHiddenDisplay == 'none'){
    hidden.style.display = 'block';
    toggleClass(t,c);
    return true;
  } else {
    hidden.style.display = 'none';
    toggleClass(t,c);
    return false;
  }
}

function toggleClass(element, className){
    if (!element || !className){
        return;
    }
    
    var classString = element.className, nameIndex = classString.indexOf(className);
    if (nameIndex == -1) {
        classString += ' ' + className;
    }
    else {
        classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
    }
    element.className = classString;
}

addMessage = function(data){
  var ul = document.getElementById('user_messages_{{$id}}');
  var li = document.createElement("li");
  li.innerHTML = data.server.data.message;
  ul.appendChild(li);
}

displayInit = function (e) {
  var status = qwest.get('/status/{{$id}}', null, {cache:true});
  status.success(function(d){
    display(e,link,'');
    if(d.online){
      var ws = new WebSocket('ws://localhost:8080');
    var socket = new BrainSocket(
            ws,
            new BrainSocketPubSub()
    );

    socket.Event.listen('app.success',function(data){
            console.log('An app success message was sent from the ws server!');
            console.log(data);
          });
      socket.Event.listen('generic.event',function(data){
            console.log('An app success message was sent from the ws server!');
            addMessage(data);
          });

    waitWS(ws, function(){
      socket.success('lol');
    });
      e.innerHTML = '<ul id="user_messages_{{$id}}"></ul><textarea></textarea><button>Enviar</button>';
    }
  });
}